package thkoeln.dungeon.player.core.events.concreteevents.game;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import thkoeln.dungeon.player.core.events.AbstractEvent;
import thkoeln.dungeon.player.game.domain.GameStatus;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class GameStatusEvent extends AbstractEvent {
    private UUID gameId;
    private UUID gameworldId;
    private GameStatus status;

    @Override
    @JsonIgnore
    public boolean isValid() {
        return ( gameId != null && status != null );
    }

    @Override
    public String toStringShort() {
        return super.toStringShort() + " (" + status + ")";
    }
}
