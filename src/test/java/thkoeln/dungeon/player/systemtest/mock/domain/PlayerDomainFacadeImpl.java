package thkoeln.dungeon.player.systemtest.mock.domain;

import org.springframework.stereotype.Service;

import java.util.UUID;

//TODO Implement the service methods
@Service
public class PlayerDomainFacadeImpl implements PlayerDomainFacade {
    @Override
    public <T> Integer getBalanceOfPlayer(T player) {
        return null;
    }

    @Override
    public <T> void setBalanceForPlayer(T player, int balance) {

    }

    @Override
    public <T> T findPlayerByPlayerId(UUID playerId) {
        return null;
    }
}
