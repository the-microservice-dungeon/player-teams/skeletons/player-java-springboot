package thkoeln.dungeon.player.systemtest.mock.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Abstract superclass of the test scenario configuration dtos.
 */
@Getter
@NoArgsConstructor
public abstract class TestScenarioSettings {
}
