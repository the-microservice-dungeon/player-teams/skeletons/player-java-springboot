package thkoeln.dungeon.player.systemtest.mock.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Domain Facade is used in the mock tests to access parts of the domain that does not exist yet. Domain objects like
 * robot, planet, etc. are intentionally incomplete in this skeleton, therefore their access has to indirect.
 *
 * If you want to successfully start the mock tests, please go through the multiple sub domain facade interfaces and
 * complete their implementation. They mostly contain CRUD operations that can be implemented vai a single line of code.
 */
@Service
public class DomainFacade {
    public final ResetDomainFacade resetDomainFacade;
    public final GameDomainFacade gameDomainFacade;
    public final PlayerDomainFacade playerDomainFacade;
    public final TradableDomainFacade tradableDomainFacade;
    public final PlanetDomainFacade planetDomainFacade;
    public final RobotDomainFacade robotDomainFacade;

    @Autowired
    public DomainFacade(ResetDomainFacade resetDomainFacade, GameDomainFacade gameDomainFacade, PlayerDomainFacade playerDomainFacade, TradableDomainFacade tradableDomainFacade, PlanetDomainFacade planetDomainFacade, RobotDomainFacade robotDomainFacade) {
        this.resetDomainFacade = resetDomainFacade;
        this.gameDomainFacade = gameDomainFacade;
        this.playerDomainFacade = playerDomainFacade;
        this.tradableDomainFacade = tradableDomainFacade;
        this.planetDomainFacade = planetDomainFacade;
        this.robotDomainFacade = robotDomainFacade;
    }

}
