package thkoeln.dungeon.player.systemtest.mock.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

/**
 * Contains the configuration data of an enemy robot.
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class EnemyRobotDto {

    private UUID id;

    private PlanetConfigDto planet;

    private Integer healthLevel;
    private Integer energyLevel;
    private Integer energyRegenLevel;
    private Integer damageLevel;
    private Integer miningLevel;
    private Integer miningSpeedLevel;

    private Integer health;
    private Integer energy;

    private List<NextOrder> nextOrders;

}
