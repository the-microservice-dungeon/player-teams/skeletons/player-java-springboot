package thkoeln.dungeon.player.systemtest.mock.domain;

import thkoeln.dungeon.player.core.events.concreteevents.game.RoundStatusType;
import thkoeln.dungeon.player.game.domain.Game;

import java.util.UUID;

/**
 * Contains necessary CRUD operations for the game domain object.
 */
public interface GameDomainFacade {

    /**
     * @param game
     * @return the round status of the currently active round in the given game.
     */
    public RoundStatusType getRoundStatusForCurrentRound(Game game);

    /**
     * @param gameId
     * @return the game with the given game id.
     */
    public Game findGameByGameId(UUID gameId);

}
