package thkoeln.dungeon.player.systemtest.mock.domain;

import org.springframework.stereotype.Service;

//TODO Implement the service methods
@Service
public class ResetDomainFacadeImpl implements ResetDomainFacade {
    @Override
    public void resetEverything() {

    }

    @Override
    public void resetEverythingExceptPlayer() {

    }
}
