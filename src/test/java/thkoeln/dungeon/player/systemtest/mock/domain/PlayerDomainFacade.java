package thkoeln.dungeon.player.systemtest.mock.domain;

import java.util.UUID;

/**
 * Contains necessary CRUD operations for the player domain object.
 */
public interface PlayerDomainFacade {

    /**
     * @param player
     * @param <T>
     * @return the money balance of the given player
     */
    public <T> Integer getBalanceOfPlayer(T player);

    /**
     * Set the money balance for the given player
     * @param player
     * @param balance
     * @param <T>
     */
    public <T> void setBalanceForPlayer(T player, int balance);

    /**
     * @param playerId
     * @param <T>
     * @return the player with the given player id
     */
    public <T> T findPlayerByPlayerId(UUID playerId);

}
