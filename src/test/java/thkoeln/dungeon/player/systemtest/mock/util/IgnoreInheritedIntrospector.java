package thkoeln.dungeon.player.systemtest.mock.util;

import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import thkoeln.dungeon.player.core.events.AbstractEvent;

/**
 * Can be added to the jackson object mapper to have him ignore inherited properties during the serialization process.
 * This can be useful if you want to serialize the event dtos in order to use them to request events from the mock service.
 *
 *
 * Usage: objectMapper.setAnnotationIntrospector(new IgnoreInheritedIntrospector());
 */
public class IgnoreInheritedIntrospector extends JacksonAnnotationIntrospector {

    @Override
    public boolean hasIgnoreMarker(final AnnotatedMember m) {
        return m.getDeclaringClass() == AbstractEvent.class || super.hasIgnoreMarker(m);
    }

}
