package thkoeln.dungeon.player.systemtest.mock.domain;

import org.springframework.stereotype.Service;
import thkoeln.dungeon.player.core.events.concreteevents.game.RoundStatusType;
import thkoeln.dungeon.player.game.domain.Game;

import java.util.UUID;

//TODO Implement the service methods
@Service
public class GameDomainFacadeImpl implements GameDomainFacade {

    @Override
    public RoundStatusType getRoundStatusForCurrentRound(Game game) {
        return null;
    }

    @Override
    public Game findGameByGameId(UUID gameId) {
        return null;
    }
}
