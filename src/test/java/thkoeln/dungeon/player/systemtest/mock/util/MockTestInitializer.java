package thkoeln.dungeon.player.systemtest.mock.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import thkoeln.dungeon.player.core.restadapter.PlayerRegistryDto;
import thkoeln.dungeon.player.systemtest.mock.domain.DomainFacade;
import thkoeln.dungeon.player.player.domain.Player;
import thkoeln.dungeon.player.player.domain.PlayerRepository;

/**
 * Creates and registers the player with the mock service.
 *
 * Used only with the "mock"-profile active.
 */
@Component
@Profile("mock")
public class MockTestInitializer implements InitializingBean {
    private static final Logger logger = LoggerFactory.getLogger(MockTestInitializer.class);
    private static final ObjectMapper objectMapper = new ObjectMapper();

    private final DomainFacade domainFacade;
    private final PlayerRepository playerRepository;

    @Autowired
    private RestTemplate restTemplate;

    private final String mockHost;

    @Autowired
    public MockTestInitializer(DomainFacade domainFacade, PlayerRepository playerRepository,
                               @Value("${dungeon.mock.host}") String mockHost) {
        this.domainFacade = domainFacade;
        this.playerRepository = playerRepository;
        this.mockHost = mockHost;
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        domainFacade.resetDomainFacade.resetEverything();

        Player player = Player.ownPlayer("mock-name", "mock-email@msd.com");
        playerRepository.save(player);

        PlayerRegistryDto requestDto = new PlayerRegistryDto();
        requestDto.setName(player.getName());
        requestDto.setEmail(player.getEmail());

        String jsonRequest = objectMapper.writeValueAsString(requestDto);
        logger.info("Requested registration of player: " + jsonRequest);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        ResponseEntity<String> response = null;

        try {
            response = restTemplate.postForEntity(mockHost + "/players",
                    new HttpEntity<>(jsonRequest, headers), String.class);
        } catch (org.springframework.web.client.HttpClientErrorException e) {
            response = restTemplate.getForEntity(mockHost + "/players?name=mock-name&mail=mock-email@msd.com",
                    String.class);
        }

        logger.info("Http response: " + response.getBody());

        PlayerRegistryDto responseDto = objectMapper.readValue(response.getBody(), PlayerRegistryDto.class);

        player.assignPlayerId(responseDto.getPlayerId());
        player.setPlayerExchange(responseDto.getPlayerExchange());
        player.setPlayerQueue(responseDto.getPlayerQueue());
        playerRepository.save(player);
    }

}
