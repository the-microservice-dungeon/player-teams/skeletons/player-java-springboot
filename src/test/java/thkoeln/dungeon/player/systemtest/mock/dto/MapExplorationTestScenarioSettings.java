package thkoeln.dungeon.player.systemtest.mock.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Contains the configuration data of a map exploration test scenario.
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MapExplorationTestScenarioSettings extends TestScenarioSettings {

    private Integer mapSize;
    private Boolean plentiful;
    private Integer explorerAmount;

}
