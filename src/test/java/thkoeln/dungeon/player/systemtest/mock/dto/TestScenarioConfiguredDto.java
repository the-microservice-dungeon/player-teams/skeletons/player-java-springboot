package thkoeln.dungeon.player.systemtest.mock.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

/**
 * Contains the response data after successfully configuring a test scenario.
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TestScenarioConfiguredDto {
    UUID gameId;
    TestScenario testScenario;
}
