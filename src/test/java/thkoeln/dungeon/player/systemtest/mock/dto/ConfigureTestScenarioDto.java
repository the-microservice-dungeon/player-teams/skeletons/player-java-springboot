package thkoeln.dungeon.player.systemtest.mock.dto;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Contains the configuration data for the selected test scenario.
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ConfigureTestScenarioDto {
    private TestScenario testScenario;

    @JsonTypeInfo(
            use = JsonTypeInfo.Id.NAME,
            property = "testScenario",
            include = JsonTypeInfo.As.EXTERNAL_PROPERTY
    )
    @JsonSubTypes({
            @JsonSubTypes.Type(value = FightTestScenarioSettings.class, name = "fight"),
            @JsonSubTypes.Type(value = MapExplorationTestScenarioSettings.class, name = "map_exploration")
    })
    private TestScenarioSettings testScenarioSettings;
}
