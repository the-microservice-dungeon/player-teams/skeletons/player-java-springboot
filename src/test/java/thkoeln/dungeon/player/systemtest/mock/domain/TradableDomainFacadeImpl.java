package thkoeln.dungeon.player.systemtest.mock.domain;

import org.springframework.stereotype.Service;
import thkoeln.dungeon.player.core.domainprimitives.purchasing.TradeableType;

import java.util.List;

//TODO Implement the service methods
@Service
public class TradableDomainFacadeImpl implements TradableDomainFacade {
    @Override
    public <T> List<T> getAllTradableItems() {
        return null;
    }

    @Override
    public <T> T getTradableItemByName(String name) {
        return null;
    }

    @Override
    public <T> Integer getPriceOfTradableItem(T tradableItem) {
        return null;
    }

    @Override
    public <T> TradeableType getTradableTypeOfTradableItem(T tradableItem) {
        return null;
    }
}
