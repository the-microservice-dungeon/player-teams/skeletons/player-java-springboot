package thkoeln.dungeon.player.systemtest.mock.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Contains the configuration data for a fight test scenario.
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class FightTestScenarioSettings extends TestScenarioSettings {

    private Integer mapSize;
    private List<PlanetConfigDto> planets;

    private PlayerConfigDto player;
    private PlayerConfigDto enemy;

    private List<FriendlyRobotDto> friendlyRobots;
    private List<EnemyRobotDto> enemyRobots;

}
