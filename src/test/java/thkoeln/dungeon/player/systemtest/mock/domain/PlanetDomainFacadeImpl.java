package thkoeln.dungeon.player.systemtest.mock.domain;

import org.springframework.stereotype.Service;
import thkoeln.dungeon.player.core.domainprimitives.location.CompassDirection;
import thkoeln.dungeon.player.core.domainprimitives.location.MineableResourceType;

import java.util.Map;
import java.util.UUID;

//TODO Implement the service methods
@Service
public class PlanetDomainFacadeImpl implements PlanetDomainFacade {
    @Override
    public <T> T createNewPlanet() {
        return null;
    }

    @Override
    public <T> void savePlanet(T planet) {

    }

    @Override
    public <T> T getPlanetByPlanetId(UUID id) {
        return null;
    }

    @Override
    public <T> UUID getPlanetIdOfPlanet(T planet) {
        return null;
    }

    @Override
    public <T> Map<CompassDirection, T> getNeighboursOfPlanet(T planet) {
        return null;
    }

    @Override
    public <T> int getMovementDifficultyForPlanet(T planet) {
        return 0;
    }

    @Override
    public <T> MineableResourceType getResourceTypeOfPlanet(T planet) {
        return null;
    }

    @Override
    public <T> Integer getCurrentResourceAmountOfPlanet(T planet) {
        return null;
    }

    @Override
    public <T> Integer getMaxResourceAmountOfPlanet(T planet) {
        return null;
    }

    @Override
    public <T> int getXCoordOfPlanet(T planet) {
        return 0;
    }

    @Override
    public <T> int getYCoordOfPlanet(T planet) {
        return 0;
    }

    @Override
    public <T> T getRandomNeighbourOfPlanet(T planet) {
        return null;
    }

    @Override
    public <T> void setPlanetIdForPlanet(T planet, UUID planetId) {

    }

    @Override
    public <T> void setResourceTypeForPlanet(T planet, MineableResourceType type) {

    }

    @Override
    public <T> void setCurrentResourceAmountForPlanet(T planet, int currentAmount) {

    }

    @Override
    public <T> void setMaxResourceAmountForPlanet(T planet, int maxAmount) {

    }

    @Override
    public <T> void setCoordinatesForPlanet(T planet, int xCoord, int yCoord) {

    }

    @Override
    public <T> void setMovementDifficultyForPlanet(T planet, int movementDifficulty) {

    }
}
